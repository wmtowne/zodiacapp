package edu.svsu.zodiacapp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class HoroscopeActivity extends AppCompatActivity {

    public static final String H_NO = "H_NO";
    private DailyHoroscope zodiacApi = new DailyHoroscope();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horoscope);
        int hsNo = (Integer) getIntent().getExtras().get(H_NO);
        //Horoscope horoscope = Horoscope.horoscopes[hsNo];

        try {

            SQLiteOpenHelper zodiacDatabaseHelper = new ZodiacDatabaseHelper(this);
            SQLiteDatabase db = zodiacDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.query("HOROSCOPE", new String[] {"NAME", "DESCRIPTION", "SYMBOL", "MONTH"}, "_id = ?", new String[] {Integer.toString(hsNo)}, null, null, null);

            if(cursor.moveToFirst()) {

                //Get the drink details from the server
                String nameText = cursor.getString(0);
                String descriptionText = cursor.getString(1);
                String symbolText = cursor.getString(2);
                String monthText = cursor.getString(3);
                String dailyHoroscope = zodiacApi.getDailyHoroscope(nameText);

                TextView name = (TextView) findViewById(R.id.name);
                name.setText(nameText);


                TextView description = (TextView) findViewById(R.id.description);
                description.setText(descriptionText);


                TextView symbol = (TextView) findViewById(R.id.symbol);
                symbol.setText(symbolText);


                TextView month = (TextView) findViewById(R.id.month);
                month.setText(monthText);

                TextView daily = (TextView) findViewById(R.id.daily);
                daily.setText(dailyHoroscope);

            }

            cursor.close();
            db.close();



        }catch(SQLiteException ex){
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}

