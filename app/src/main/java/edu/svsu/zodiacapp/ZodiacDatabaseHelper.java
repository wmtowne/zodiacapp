package edu.svsu.zodiacapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

//Re-Written db helper class
public class ZodiacDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "zodiac";
    private static final int DB_VERSION = 1;

    ZodiacDatabaseHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        updateMyDatabase(sqLiteDatabase, 0, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        updateMyDatabase(sqLiteDatabase, oldVersion, newVersion);
    }


    public void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion){
        if(oldVersion <= 1) {
            db.execSQL("CREATE TABLE HOROSCOPE (_id INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, DESCRIPTION TEXT, SYMBOL TEXT, MONTH TEXT);");
            insertHoroscope(db, "Aries", "Courageous and Energetic", "Ram", "April");
            insertHoroscope(db, "Taurus", "Known for being reliable, practical, ambitious, and sensual", "Bull", "May");
            insertHoroscope(db, "Gemini", "Gemini-born are clever and intellectual", "Twins", "June");
            insertHoroscope(db, "Cancer", "Tenacious, loyal and sympathetic.", "Crab", "July");
            insertHoroscope(db, "Leo", "Warm, action-oriented and driven by the desire to be loved and admired.", "Lion", "August");
            insertHoroscope(db, "Virgo", "Methodical, meticulous, analytical and mentally astute.", "Virgin", "September");
            insertHoroscope(db, "Libra", "Librans are famous for maintaining balance and harmony.", "Scales", "October");
            insertHoroscope(db, "Scorpio", "String willed and mysterious.", "Scorpion", "November");
            insertHoroscope(db, "Sagittarius", "Born adventurers", "Archer", "December");
            insertHoroscope(db, "Capicorn", "The most determined sign in the Zodiac", "Goat", "January");
            insertHoroscope(db, "Aquarius", "Humanitarians to the core", "Water Bearer", "February");
            insertHoroscope(db, "Pisces", "Proverbial dreamers of the Zodiac", "Fish", "March");
        }
    }



    private static void insertHoroscope(SQLiteDatabase db, String name, String description, String symbol, String month){

        ContentValues zodiacValues = new ContentValues();
        zodiacValues.put("NAME", name);
        zodiacValues.put("DESCRIPTION", description);
        zodiacValues.put("SYMBOL", symbol);
        zodiacValues.put("MONTH", month);
        db.insert("HOROSCOPE", null, zodiacValues);

    }


}
